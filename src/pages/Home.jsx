import Acceuil from "../components/Acceuil";
import ListeJeux from "../components/ListeJeux";
import DetailJeux from "../components/DetailJeux";

export default function Home() {
  return (
    <header className="App-header">
      <Acceuil />
      <ListeJeux />
      <DetailJeux />
    </header>
  );
}
