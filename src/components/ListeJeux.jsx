import React, { useEffect, useState } from "react";
import './ListeJeux.css';
import DetailJeux from "../components/DetailJeux";

class Filter extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            filterText: ""
        }
        this.handleFilterTextChange = this.handleFilterTextChange.bind(this)
    }

    handleFilterTextChange(filterText) {
        this.setState({ filterText })
    }

    render() {
        const { items } = this.props
        return <React.Fragment>
            <SearchBar
                filterText={this.state.filterText}
                onFilterTextChange={this.handleFilterTextChange}
            />
            <List
                items={items}
                filterText={this.state.filterText}
            />
        </React.Fragment>
    }
}
class SearchBar extends React.Component {

    constructor(props) {
        super(props)
        this.handleFilterTextChange = this.handleFilterTextChange.bind(this)
    }

    handleFilterTextChange(e) {
        this.props.onFilterTextChange(e.target.value)
    }

    render() {
        const { filterText } = this.props
        //        console.log("value :", filterText)
        return < div className="form-group" >
            <input type="text" value={filterText} className="form-control" placeholder="Rechercher un Jeux" onChange={this.handleFilterTextChange} />
        </div >
    }
}

function ListRow({ item, evnt }) {
    return <div onClick={() => handleClick(evnt)}>
        <div className="align">
            <img src={item.background_image} className="preview" />
            <h3 className="under">{item.name}</h3>
        </div>
    </div>
}

function List({ items, filterText }) {
    const rows = [];
    let lastName = null;
    let lastPreview = null;

    items.forEach(item => {
        if (item.name.indexOf(filterText) === -1) {
            return
        }
        if (item.name !== lastName) {
            lastName = item.name
            lastPreview = item.background_image
            rows.push(<ListRow key={item.name} item={item} />)
        }
    })

    //    console.log("Jeux :", rows)

    return (
        <div className="PP">
            {rows}
        </div>
    )
};

function handleClick(evnt) {
    console.log("appuie : ", evnt);
    //    <Redirect />

}

export default function ListeJeux(props) {

    const URL = "https://raw.githubusercontent.com/bastienapp/gamesapi/main/db.json";
    let firstElement
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState(null);

    useEffect(() => {
        fetch(URL)
            .then(res => res.json())
            .then(
                (data) => {
                    setIsLoaded(true);
                    setItems(data);
                },
                (err) => {
                    setIsLoaded(true);
                    setError(err);
                }
            )
    }, [])
    //    console.log("Brut :", items);

    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Chargement...</div>;
    } else {
        firstElement = items[0]
        //console.log(firstElement)
        return (
            <div className="ListeJeux">
                <Filter items={items} />
                <DetailJeux firstElement={firstElement} />
            </div>

        );
    }
}