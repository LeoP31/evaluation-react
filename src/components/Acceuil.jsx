import React from "react";
import './Acceuil.css';

export default function Acceuil(props) {

    const title = props.title || "Hunt for games";
    const description = props.description || "Avec Hunt for games, trouvez la perle rare au milieu d'un océan de jeux vidéo !";

    //    console.log("titre : ", title, ", ", "description : ", description);

    const imgURL = "../src/assets/Images/plex.png";
    return (

        <div className="Acceuil">
            <h1>Welcome to {title}</h1>
            <div className="subTitle">
                <h3>{description}</h3>
            </div>

            <img src={imgURL} className="buttonList" />
            <h3>Afficher la liste !</h3>
        </div>

    );

};